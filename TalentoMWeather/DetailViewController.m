//
//  DetailViewController.m
//  TalentoMWeather
//
//  Created by Alex on 03/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DetailViewController.h"
#import <MapKit/MapKit.h>


@interface DetailViewController () <UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *map;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *temperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *humidityWindSpeedLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@property (nonatomic, readonly) NSDictionary *location;

@end

@implementation DetailViewController

+ (DetailViewController *)viewController {
    return [[DetailViewController alloc] initWithNibName:nil bundle:nil];
}

#pragma mark - Implementation

- (void)viewDidLoad {
    [super viewDidLoad];

    
    [_presenter getWeather];
    _location = [_presenter getLocation];
    [self searchBarConfig];
    [self mapConfig];
    
    NSString * cityName = [NSString stringWithFormat:@"%@, %@",[_location objectForKey:@"name"], [_location objectForKey:@"countryCode"]];
    [_cityLabel setText:cityName];
    [_temperatureLabel setText:@"N/A ºC"];
    [_humidityWindSpeedLabel setText:@"Humidity: N/A% Wind Speed: N/A m/s"];
    [_progressBar setProgress:0 animated:YES];
    

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma SearchBar and Map config

- (void)searchBarConfig{
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat searchBarWidth = screenRect.size.width;
    
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, searchBarWidth-30, 25)];
    
    [searchBar setDelegate:self];
    
    UIView *searchBarView = [[UIView alloc] initWithFrame:searchBar.frame];
    
    [searchBarView addSubview:searchBar];
    
    UIBarButtonItem *searchBarItem =
    [[UIBarButtonItem alloc] initWithCustomView:searchBarView];
    
    self.navigationItem.leftBarButtonItem = searchBarItem;

}

- (void)mapConfig{

    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude = [[_location objectForKey:@"lat"] doubleValue];
    zoomLocation.longitude= [[_location objectForKey:@"lng"] doubleValue];
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*1609.344, 0.5*1609.344);
    
    [_map setMapType:MKMapTypeStandard];
    [_map setRegion:viewRegion animated:YES];
    [_map setBackgroundColor:[UIColor grayColor]];


}

#pragma mark - UISearchBarDelegate


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [_presenter search:[searchBar text]];
    
}



- (void) setContentView:(NSInteger)temp hum:(NSInteger)hum win:(NSInteger)win{

    dispatch_async(dispatch_get_main_queue(), ^{
        [_temperatureLabel setText:[NSString stringWithFormat:@"%ldºC", (long)temp]];
        [_humidityWindSpeedLabel setText:[NSString stringWithFormat:@"Humidity: %ld %% | WindSpeed: %ld m/s", (long)hum, (long)win]];
        
        float division = (float)temp/50;
        
        if (division < 0) {
            division = 0;
        }
        [_progressBar setProgress:division animated:YES];
        
        if(division<=0.25){
            [_progressBar setProgressTintColor:[UIColor colorWithRed:66/255 green:204/255 blue:255/255 alpha:0.8]];
        }
        else if (division>0.25 && division<=0.5){
            [_progressBar setProgressTintColor:[UIColor colorWithRed:255/255 green:187/255 blue:39/255 alpha:0.8]];

        }
        else if (division>0.5 && division<=0.75){
            [_progressBar setProgressTintColor:[UIColor colorWithRed:255/255 green:121/255 blue:45/255 alpha:0.8]];

        }
        else if (division>0.75){
            [_progressBar setProgressTintColor:[UIColor colorWithRed:191/255 green:48/255 blue:26/255 alpha:0.8]];
        }

    });
    

}






@end
