//
//  DetailInteractor.m
//  TalentoMWeather
//
//  Created by Alex on 07/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DetailInteractor.h"

#pragma mark - Implementation

@implementation DetailInteractor

#pragma mark - Instance initialization

- (void) searchWeather:(NSDictionary *)location weather:(NSMutableArray *)weatherArray{
    
    
    NSString *jsonInputString = [NSString stringWithFormat: @"north=%.01f&south=%.02f&east=%.02f&west=%.02f&username=ilgeonamessample", [[location objectForKey:@"north"] floatValue],[[location objectForKey:@"south"] floatValue],[[location objectForKey:@"east"] floatValue],[[location objectForKey:@"west"] floatValue]];
    
    NSLog(@"%@", jsonInputString);

    NSURL *url = [NSURL URLWithString:@"http://api.geonames.org/weatherJSON?"];
    
    NSURLSessionConfiguration *config =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    
    NSData *jsonData = [jsonInputString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *dataTask =
    [session uploadTaskWithRequest:request
                          fromData:jsonData
                 completionHandler:^(NSData *data, NSURLResponse *response,
                                     NSError *error) {
                     
                     if (!error) {
                         
                         NSDictionary *json =
                         [NSJSONSerialization JSONObjectWithData:data
                                                         options:0
                                                           error:nil];
                         
                         
                         NSArray * name = json[@"weatherObservations"];
                         
                         NSLog(@"%@", name);

                         
                         for(NSDictionary * dict in name){

                             NSMutableDictionary *m = [NSMutableDictionary dictionary];
                             
                             if ([dict valueForKey:@"temperature"] == nil) {
                                 [m setObject:@"0" forKey:@"temperature"];
                                 
                             }else{
                                 [m setObject:[dict valueForKey:@"temperature"] forKey:@"temperature"];
                             }
                            
                             if ([dict valueForKey:@"humidity"] == nil) {
                                 [m setObject:@"0" forKey:@"humidity"];
                                 
                             }else{
                                 [m setObject:[dict valueForKey:@"humidity"] forKey:@"humidity"];
                             }
                             
                             if ([dict valueForKey:@"windSpeed"] == nil) {
                                 [m setObject:@"0" forKey:@"windSpeed"];

                             }else{
                             [m setObject:[dict valueForKey:@"windSpeed"] forKey:@"windSpeed"];
                             }
                             
                             [weatherArray addObject:m];
                         }
                             
                             
                         
                         [_presenter setContentView];
                     }
                     
                 }];
    
    [dataTask resume];
    
}


@end
