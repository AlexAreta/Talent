//
//  SearchPresenter.m
//  TalentoMWeather
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ViewController.h"
#import "SearchPresenter.h"
#import "Wireframe.h"

@interface SearchPresenter ()


@property (nonatomic, readwrite) NSMutableArray * oldSearchArray;
@property (nonatomic, readwrite) NSString * elementOldSearch;
@property (nonatomic, readwrite) NSMutableData * responseData;

@end

@implementation SearchPresenter

#pragma mark - Implementation

- (instancetype)init {
    
    self = [super init];
    
    _oldSearchArray = [[NSMutableArray alloc] init];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"locationArray"]mutableCopy]) {
        _oldSearchArray = [[[NSUserDefaults standardUserDefaults]objectForKey:@"locationArray"]mutableCopy];
    }
    
    return self;
}


- (void)setSearchContentView:(UITableViewCell *)cell
                            indexPath:(NSIndexPath *)indexPath{
    
    NSString * cellText = [NSString stringWithFormat:@"%@, %@",[[_oldSearchArray  objectAtIndex:[indexPath row]] objectForKey:@"name"], [[_oldSearchArray  objectAtIndex:[indexPath row]] objectForKey:@"countryCode"]];

    [[cell textLabel] setText:cellText];
    
   

}
- (void)search:(NSString *)text{
        
    [_oldSearchArray removeAllObjects];
    [_interactor search:text searchItems:_oldSearchArray];

}

- (void)reloadView{

    [_view reloadTableView];
}

- (NSUInteger) sizeOldSearchArray{

    return  [_oldSearchArray count];
    
}

- (void)goToDetailView: (NSInteger)row{

    [_wireframe showDetailViewControllerFrom:(UIViewController *)_view withLocation:[_oldSearchArray objectAtIndex:row]];

}


@end
