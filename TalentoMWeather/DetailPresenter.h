//
//  DetailPresenter.h
//  TalentoMWeather
//
//  Created by Alex on 05/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailPresenterProtocol.h"
#import "Wireframe.h"
#import "DetailInteractor.h"

@interface DetailPresenter : NSObject <DetailPresenterProtocol>

@property (nonatomic, strong) Wireframe *wireframe;
@property (nonatomic, weak) DetailViewController *view;
@property (nonatomic, strong) DetailInteractor *interactor;

@end
