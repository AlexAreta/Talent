//
//  AppDelegate.h
//  TalentoMWeather
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

