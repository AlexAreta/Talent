//
//  ViewController.m
//  TalentoMWeather
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import "ViewController.h"
#import "SearchPresenterProtocol.h"
#import "SearchPresenter.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, readonly) UISearchBar *searchBar;


@end

@implementation ViewController

+ (ViewController *)viewController {
    return [[ViewController alloc] initWithNibName:nil bundle:nil];
}

#pragma mark - Implementation

- (void)viewDidLoad {
    [super viewDidLoad];

    [_tableView setDelegate:self];
    [_tableView setDataSource:self];
    
    [self searchBarConfig];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)searchBarConfig{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat searchBarWidth = screenRect.size.width-10;
    
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, searchBarWidth-20, 25)];
    
    [_searchBar setDelegate:self];
    
    UIView *searchBarView = [[UIView alloc] initWithFrame:_searchBar.frame];
    
    [searchBarView addSubview:_searchBar];
    
    UIBarButtonItem *searchBarItem =
    [[UIBarButtonItem alloc] initWithCustomView:searchBarView];
    
    self.navigationItem.leftBarButtonItem = searchBarItem;

}

- (void)reloadTableView{
    dispatch_async(dispatch_get_main_queue(), ^{
        [_tableView reloadData];
    });
}


#pragma mark - UISearchBarDelegate


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [_presenter search:[searchBar text]];
    
}

- (void)searchText:(NSString *)searchText{
    
    
    [_searchBar setText:searchText];
    [_presenter search:searchText];

}



#pragma mark - UITableViewDelegate, UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    [_presenter setSearchContentView:cell indexPath:indexPath];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_presenter sizeOldSearchArray];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [_presenter goToDetailView:[indexPath row]];
    
}


@end
