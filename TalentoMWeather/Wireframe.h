//
//  Wireframe.h
//  TalentoMWeather
//
//  Created by Alex on 05/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Wireframe : NSObject

@property (nonatomic, readwrite, strong) UIWindow *window;

- (id)initWithWindow:(UIWindow *)window;
- (void)appInit;
- (void)showDetailViewControllerFrom:(UIViewController *)viewController withLocation: (NSDictionary *)location;
- (void)showViewControllerFrom:(UIViewController *)detailViewController withSearch:(NSString *)search;

@end
