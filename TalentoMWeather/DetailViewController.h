//
//  DetailViewController.h
//  TalentoMWeather
//
//  Created by Alex on 03/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailPresenterProtocol.h"

@interface DetailViewController : UIViewController

@property (nonatomic, strong) id<DetailPresenterProtocol> presenter;

- (void) setContentView:(NSInteger)temp hum:(NSInteger)hum win:(NSInteger)win;

@end

