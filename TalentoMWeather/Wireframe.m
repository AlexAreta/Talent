//
//  Wireframe.m
//  TalentoMWeather
//
//  Created by Alex on 05/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Wireframe.h"
#import "DetailPresenter.h"
#import "DetailInteractor.h"
#import "ViewController.h"
#import "DetailViewController.h"
#import "SearchPresenter.h"
#import "SearchInteractor.h"

#pragma mark - Implementation

@implementation Wireframe

- (id)initWithWindow:(UIWindow *)window {
    
    self = [super init];
    if (self) {
        _window = window;
    }
    
    return self;
}


#pragma mark - Wireframe management

- (void)appInit{
    
    UINavigationController *navigationController;
    
    SearchInteractor *interactor = [[SearchInteractor alloc] init];
    
    ViewController *viewController = [ViewController viewController];
    
    SearchPresenter *presenter = [[SearchPresenter alloc] init];
    
    presenter.interactor = interactor;
    presenter.view = viewController;
    
    presenter.wireframe = self;
    viewController.presenter = presenter;
    interactor.presenter = presenter;
    
    navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [_window setRootViewController:navigationController];

}

- (void)showDetailViewControllerFrom:(UIViewController *)viewController withLocation: (NSDictionary *)location{
    
    UINavigationController *navigationController;
    
    DetailPresenter *presenter = [[DetailPresenter alloc] initWithLocation:location];
    DetailViewController *newViewController = [[DetailViewController alloc] init];
    DetailInteractor *interactor = [[DetailInteractor alloc] init];
    
    presenter.interactor = interactor;
    presenter.view = newViewController;
    presenter.wireframe = self;
    newViewController.presenter = presenter;
    interactor.presenter = presenter;
    
    navigationController = [[UINavigationController alloc] initWithRootViewController:newViewController];
    [_window setRootViewController:navigationController];
    
    NSLog(@"Location %@", location);
    
}

- (void)showViewControllerFrom:(UIViewController *)detailViewController withSearch:(NSString *)search{

    UINavigationController *navigationController;
    
    SearchInteractor *interactor = [[SearchInteractor alloc] init];
    
    ViewController *viewController = [ViewController viewController];
    
    SearchPresenter *presenter = [[SearchPresenter alloc] init];
    
    presenter.interactor = interactor;
    presenter.view = viewController;
    
    presenter.wireframe = self;
    viewController.presenter = presenter;
    interactor.presenter = presenter;
    
    navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
    [_window setRootViewController:navigationController];
    
    [viewController searchText:search];

}

@end
