//
//  ViewController.h
//  TalentoMWeather
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchPresenterProtocol.h"

@interface ViewController : UIViewController

@property (nonatomic, strong) id<SearchPresenterProtocol> presenter;

+ (ViewController *)viewController;
- (void)searchText:(NSString *)searchText;
- (void)reloadTableView;

@end

