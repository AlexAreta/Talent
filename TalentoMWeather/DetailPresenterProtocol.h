//
//  DetailPresenterProtocol.h
//  TalentoMWeather
//
//  Created by Alex on 05/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//


@class DetailViewController;

@protocol DetailPresenterProtocol

- (instancetype)initWithLocation:(NSDictionary *) location;
- (NSDictionary *) getLocation;
- (void)getWeather;
- (void)setContentView;
- (void)search:(NSString *)searchText;


@end
