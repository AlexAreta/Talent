//
//  DetailPresenter.m
//  TalentoMWeather
//
//  Created by Alex on 05/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DetailViewController.h"
#import "DetailPresenter.h"
#import "Wireframe.h"


@interface DetailPresenter ()

@property (nonatomic, readonly) NSDictionary *location;
@property (nonatomic, readwrite) NSMutableArray * weatherArray;


@end

@implementation DetailPresenter

#pragma mark - Implementation

- (instancetype)initWithLocation:(NSDictionary *) location {
    
    self = [super init];
    
    _weatherArray = [[NSMutableArray alloc] init];
    
    
    _location = location;
    
    NSMutableArray *mutableArray = [[[NSMutableArray alloc] init] mutableCopy];
    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"locationArray"]mutableCopy]) {
        mutableArray = [[[NSUserDefaults standardUserDefaults]objectForKey:@"locationArray"]mutableCopy];
    }
    
    [mutableArray addObject: location];
    [[NSUserDefaults standardUserDefaults]setObject:mutableArray forKey:@"locationArray"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    return self;
}


- (NSDictionary *) getLocation{

    return _location;

}

- (void)getWeather{

    [_interactor searchWeather:_location weather:_weatherArray];
   
}

- (void)setContentView{

    if ([_weatherArray count] > 0) {
            
    NSInteger temperature = 0;
    NSInteger humidity = 0;
    NSInteger windSpeed = 0;
    
    for(NSDictionary * weather in _weatherArray){
    
        temperature = [[weather objectForKey:@"temperature"] integerValue] + temperature;
        humidity = [[weather objectForKey:@"humidity"] integerValue] + humidity;
        windSpeed = [[weather objectForKey:@"windSpeed"] integerValue] + windSpeed;
    
    }
        
    [_view setContentView:temperature/[_weatherArray count] hum:humidity/[_weatherArray count] win:windSpeed/[_weatherArray count]];
    }
}

- (void)search:(NSString *)searchText{

    [_wireframe showViewControllerFrom:(UIViewController *)_view withSearch:searchText];

}


@end
