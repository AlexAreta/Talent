//
//  SearchPresenterProtocol.h
//  TalentoMWeather
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

@class ViewController;

@protocol SearchPresenterProtocol

- (void)search:(NSString*)text;
- (void)setSearchContentView:(UITableViewCell *)cell
                            indexPath:(NSIndexPath *)indexPath;
- (NSUInteger) sizeOldSearchArray;
- (void) reloadView;
- (void) goToDetailView:(NSInteger)row;

@end
