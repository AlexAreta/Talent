//
//  SearchInteractor.m
//  TalentoMWeather
//
//  Created by Alex on 07/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//
#import <Foundation/Foundation.h>

#import "SearchInteractor.h"

#pragma mark - Implementation

@implementation SearchInteractor

#pragma mark - Instance initialization

- (void) search:(NSString *)textSearch searchItems:(NSMutableArray *)searchItems{

    
    NSString *jsonInputString = [NSString stringWithFormat: @"q=%@&maxRows=20&startRow=0&lang=en&isNameRequired=true&style=FULL&username=ilgeonamessample", textSearch];
    
    NSURL *url = [NSURL URLWithString:@"http://api.geonames.org/searchJSON?"];
    
    NSURLSessionConfiguration *config =
    [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"POST";
    
    NSData *jsonData = [jsonInputString dataUsingEncoding:NSUTF8StringEncoding];
    
    NSURLSessionUploadTask *dataTask =
    [session uploadTaskWithRequest:request
                          fromData:jsonData
                 completionHandler:^(NSData *data, NSURLResponse *response,
                                     NSError *error) {
                     
                     if (!error) {
                         
                         NSDictionary *json =
                         [NSJSONSerialization JSONObjectWithData:data
                                                         options:0
                                                           error:nil];
                         
                         
                         NSArray * name = json[@"geonames"];
                         
                         NSLog(@"%@", name);
                         
                         for(NSDictionary * dict in name){
                             
                             NSMutableDictionary *bbox = [dict valueForKey:@"bbox"];
                             
                             if (bbox!=nil) {
                                 
                                 NSMutableDictionary *m = [NSMutableDictionary dictionary];
                                 
                                 [m setObject:[dict valueForKey:@"name"] forKey:@"name"];
                                 if ([dict valueForKey:@"countryCode"]) {
                                     [m setObject:[dict valueForKey:@"countryCode"] forKey:@"countryCode"];
                                 }
                                 [m setObject:[dict valueForKey:@"lat"] forKey:@"lat"];
                                 [m setObject:[dict valueForKey:@"lng"] forKey:@"lng"];
                                 [m setObject:[bbox valueForKey:@"east"] forKey:@"east"];
                                 [m setObject:[bbox valueForKey:@"west"] forKey:@"west"];
                                 [m setObject:[bbox valueForKey:@"north"] forKey:@"north"];
                                 [m setObject:[bbox valueForKey:@"south"] forKey:@"south"];
                                 
                                 
                                 [searchItems addObject:m];
                             }
                             
                             
                             
                         }
                         
                         
                         [_presenter reloadView];

                         
                     }
                     
                 }];
    
    [dataTask resume]; // Executed First
    
}


@end
