//
//  SearchPresenter.h
//  TalentoMWeather
//
//  Created by Alex on 01/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchPresenterProtocol.h"
#import "Wireframe.h"
#import "SearchInteractor.h"

@interface SearchPresenter : NSObject <SearchPresenterProtocol>

@property (nonatomic, strong) Wireframe *wireframe;
@property (nonatomic, weak) ViewController *view;
@property (nonatomic, strong) SearchInteractor *interactor;

@end
