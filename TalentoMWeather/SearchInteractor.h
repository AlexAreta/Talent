//
//  SearchInteractor.h
//  TalentoMWeather
//
//  Created by Alex on 07/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchPresenterProtocol.h"

@interface SearchInteractor : NSObject

@property (nonatomic, strong) id<SearchPresenterProtocol> presenter;

- (void) search:(NSString *)textSearch searchItems:(NSMutableArray *)searchItems;

@end
