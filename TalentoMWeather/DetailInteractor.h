//
//  DetailInteractor.h
//  TalentoMWeather
//
//  Created by Alex on 07/05/2017.
//  Copyright © 2017 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DetailPresenterProtocol.h"

@interface DetailInteractor : NSObject

@property (nonatomic, strong) id<DetailPresenterProtocol> presenter;

- (void) searchWeather:(NSDictionary *)location weather:(NSMutableArray *)weatherArray;


@end
